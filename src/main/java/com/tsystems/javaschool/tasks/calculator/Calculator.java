package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class Calculator {

    private int isDivideByZero = 0;

    private Deque<Character> stackOfOperations = new ArrayDeque<>();
    private Deque<Double> stackOfOperands = new ArrayDeque<>();

    private ArrayList<String> digits;

    private DecimalFormatSymbols dot = new DecimalFormatSymbols(Locale.US);
    private DecimalFormat fourDigits = new DecimalFormat("##.####", dot);

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        int countOfDigits = 0;
        if (statement == null || statement.equals("")) {
            return null;
        }
        if (!separateDigits(statement)) {
            return null;
        }
        if (!checkStatement(statement) || !isCorrect(statement)) {
            return null;
        } else {
            for (int i = 0; i < statement.length(); i++) {
                char charOfStatement = statement.charAt(i);
                if (i == statement.length() - 1) {
                    if (Character.isDigit(charOfStatement)) {
                        stackOfOperands.push(Double.parseDouble(digits.get(countOfDigits)));
                        lastOperation(stackOfOperands, stackOfOperations);
                        break;
                    } else {
                        stackOfOperations.push(charOfStatement);
                        lastOperation(stackOfOperands, stackOfOperations);
                        if (isDivideByZero == 1) {
                            return null;
                        }
                        break;
                    }
                }
                if (Character.isDigit(charOfStatement)) {
                    if (Character.toString(statement.charAt(i + 1)).equals(".")) {
                        continue;
                    }
                    if (Character.isDigit(statement.charAt(i + 1))) {
                        continue;
                    }
                    stackOfOperands.push(Double.parseDouble(digits.get(countOfDigits)));
                    countOfDigits++;
                }
                switch (charOfStatement) {
                    case '(':
                        stackOfOperations.push(charOfStatement);
                        break;
                    case ')':
                        statementWithBrackets(stackOfOperands, stackOfOperations);
                        stackOfOperations.pop();
                        break;
                    case '+':
                        if (stackOfOperations.size() != 0) {
                            plusAndMinusCalculations(stackOfOperands, stackOfOperations);
                        }
                        stackOfOperations.push(charOfStatement);
                        break;
                    case '-':
                        if (stackOfOperations.size() != 0) {
                            plusAndMinusCalculations(stackOfOperands, stackOfOperations);
                        }
                        stackOfOperations.push(charOfStatement);
                        break;
                    case '*':
                        if (stackOfOperations.size() != 0) {
                            if (isPlusOrMinus(stackOfOperations)) {
                                stackOfOperations.push(charOfStatement);
                                break;
                            } else {
                                multiplyAndDivisionCalculations(stackOfOperands, stackOfOperations);
                            }
                        }
                        stackOfOperations.push(charOfStatement);
                        break;
                    case '/':
                        if (stackOfOperations.size() != 0) {
                            if (isPlusOrMinus(stackOfOperations)) {
                                stackOfOperations.push(charOfStatement);
                                break;
                            } else {
                                multiplyAndDivisionCalculations(stackOfOperands, stackOfOperations);
                                if (isDivideByZero == 1) {
                                    return null;
                                }
                            }
                        }
                        stackOfOperations.push(charOfStatement);
                        break;
                }
            }
        }
        return isDouble(stackOfOperands);
    }

    private String isDouble(Deque<Double> stackOfOperands) {

        if (stackOfOperands.peek().toString().matches("[+\\-]?\\d*(\\.0)")) {
            return stackOfOperands.peek().intValue() + "";
        } else {
            return fourDigits.format(stackOfOperands.peek());
        }
    }

    private boolean separateDigits(String statement) {
        digits = new ArrayList<>(Arrays.asList(statement.split("[(|)|+|*|\\-|/]")));
        for (int i = 0; i < digits.size(); i++) {
            if (!digits.get(i).matches("[+\\-]?\\d*(\\.\\d+)?")) {
                return false;
            }
            if (digits.get(i).equals("")) {
                digits.remove(i);
            }
        }
        return true;
    }

    private void statementWithBrackets(Deque<Double> stackOfOperands,
                                       Deque<Character> stackOfOperations) {
        while (!stackOfOperations.peek().equals('(')) {
            if (stackOfOperations.peek().equals('+')) {
                stackOfOperands.push(summ(stackOfOperands));
                stackOfOperations.pop();
                continue;
            }
            if (stackOfOperations.peek().equals('-')) {
                stackOfOperands.push(substraction(stackOfOperands));
                stackOfOperations.pop();
                continue;
            }
            if (stackOfOperations.peek().equals('*')) {
                stackOfOperands.push(multiply(stackOfOperands));
                stackOfOperations.pop();
                continue;
            }
            if (stackOfOperations.peek().equals('/')) {
                stackOfOperands.push(division(stackOfOperands));
                stackOfOperations.pop();
            }
        }
    }

    private boolean isCorrect(String statement) {
        return statement.matches("^[0-9*\\-+/.()]*");
    }

    private boolean checkStatement(String statement) {
        Deque<Character> bracketsCheckingStack = new ArrayDeque<>();
        Deque<Character> signsCheckingStack = new ArrayDeque<>();
        boolean flag = true;

        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            switch (ch) {
                case '+':
                    if (signsCheckingStack.size() != 0) {
                        if (signsCheckingStack.peek() == ch) {
                            return false;
                        }
                    }
                    signsCheckingStack.push(ch);
                    break;
                case '-':
                    if (signsCheckingStack.size() != 0) {
                        if (signsCheckingStack.peek() == ch) {
                            return false;
                        }
                    }
                    signsCheckingStack.push(ch);
                    break;
                case '/':
                    if (signsCheckingStack.size() != 0) {
                        if (signsCheckingStack.peek() == ch) {
                            return false;
                        }
                    }
                    signsCheckingStack.push(ch);
                    break;
                case '*':
                    if (signsCheckingStack.size() != 0) {
                        if (signsCheckingStack.peek() == ch) {
                            return false;
                        }
                    }
                    signsCheckingStack.push(ch);
                    break;
                case '(':
                    if (bracketsCheckingStack.size() != 0) {
                        if (bracketsCheckingStack.peek() == ch) {
                            return false;
                        }
                    }
                    bracketsCheckingStack.push(ch);
                    break;
                case ')':
                    if (bracketsCheckingStack.size() == 0) {
                        return false;
                    } else {
                        if (ch == bracketsCheckingStack.pop()) {
                            return false;
                        }
                    }
                    break;
            }
        }
        return flag;
    }

    private double multiply(Deque<Double> stackOfOperands) {
        double firstOperand = stackOfOperands.pop();
        double secondOperand = stackOfOperands.pop();
        return firstOperand * secondOperand;
    }

    private double division(Deque<Double> stackOfOperands) {
        double firstOperand = stackOfOperands.pop();
        double secondOperand = stackOfOperands.pop();
        return secondOperand / firstOperand;
    }

    private double summ(Deque<Double> stackOfOperands) {
        double firstOperand = stackOfOperands.pop();
        double secondOperand = stackOfOperands.pop();
        return firstOperand + secondOperand;
    }

    private double substraction(Deque<Double> stackOfOperands) {
        double firstOperand = stackOfOperands.pop();
        double secondOperand = stackOfOperands.pop();
        return secondOperand - firstOperand;
    }

    private boolean isPlusOrMinus(Deque<Character> stackOfOperations) {
        char currentOperationValue = stackOfOperations.peek();
        return currentOperationValue == '+' || currentOperationValue == '-';
    }

    private void plusAndMinusCalculations(Deque<Double> stackOfOperands,
                                          Deque<Character> stackOfOperations) {
        char currentOperationValue = stackOfOperations.peek();
        if (currentOperationValue == '(') {
            return;
        }
        switch (currentOperationValue) {
            case '*':
                stackOfOperands.push(multiply(stackOfOperands));
                stackOfOperations.pop();
                break;
            case '/':
                stackOfOperands.push(division(stackOfOperands));
                stackOfOperations.pop();
                break;
            case '+':
                stackOfOperands.push(summ(stackOfOperands));
                stackOfOperations.pop();
                break;
            default:
                stackOfOperands.push(substraction(stackOfOperands));
                stackOfOperations.pop();
                break;
        }
        if (stackOfOperations.size() != 0) {
            plusAndMinusCalculations(stackOfOperands, stackOfOperations);
        }
    }

    private void multiplyAndDivisionCalculations(Deque<Double> stackOfOperands,
                                                 Deque<Character> stackOfOperations) {
        char currentOperationValue = stackOfOperations.peek();
        if (currentOperationValue == '*') {
            stackOfOperands.push(multiply(stackOfOperands));
            stackOfOperations.pop();
        } else if (currentOperationValue == '/') {
            if (stackOfOperands.peek().equals(0.0)) {
                isDivideByZero = 1;
                return;
            }
            stackOfOperands.push(division(stackOfOperands));
            stackOfOperations.pop();
        }
    }

    private void lastOperation(Deque<Double> stackOfOperands,
                               Deque<Character> stackOfOperations) {
        char currentOperationValue = stackOfOperations.peek();
        switch (currentOperationValue) {
            case ')':
                stackOfOperations.pop();
                statementWithBrackets(stackOfOperands, stackOfOperations);
                stackOfOperations.pop();
                break;
            case '+':
                stackOfOperands.push(summ(stackOfOperands));
                stackOfOperations.pop();
                break;
            case '-':
                stackOfOperands.push(substraction(stackOfOperands));
                stackOfOperations.pop();
                break;
            case '*':
                stackOfOperands.push(multiply(stackOfOperands));
                stackOfOperations.pop();
                break;
            case '/':
                if (stackOfOperands.peek().equals(0.0)) {
                    isDivideByZero = 1;
                    return;
                }
                stackOfOperands.push(division(stackOfOperands));
                stackOfOperations.pop();
                break;
        }
        if (stackOfOperations.size() != 0) {
            lastOperation(stackOfOperands, stackOfOperations);
        }
    }
}