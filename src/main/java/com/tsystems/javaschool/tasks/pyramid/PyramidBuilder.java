package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private int height = 0;
    private int base = 0;
    private int switcher;
    private int digit;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        isPyramidValid(inputNumbers);
        Collections.sort(inputNumbers);
        return drawPyramid(inputNumbers);
    }

    private void isPyramidValid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        double validPyramid = inputNumbers.size() * 8 + 1;
        if (Math.sqrt(validPyramid) != (int) Math.sqrt(validPyramid)) {
            throw new CannotBuildPyramidException();
        } else {
            height = (int) Math.sqrt(validPyramid) / 2;
            base = findBase(inputNumbers);
            digit = inputNumbers.size() - 1;
        }
    }

    private int findBase(List<Integer> inputNumbers) {
        int discr = 1 + 8 * inputNumbers.size();
        int x = (int) (-1 + Math.sqrt(discr)) / 2;
        return 2 * x - 1;
    }

    private int[][] drawPyramid(List<Integer> inputNumbers) {
        int[][] pyramid = new int[height][base];
        int baseForTriangle = base - 1;
        int countOfZero = 0;

        for (int i = height - 1; i >= 0; i--) {
            switcher = 0;
            for (int j = baseForTriangle; j >= countOfZero; j--) {
                if (switcher == 0) {
                    pyramid[i][j] = inputNumbers.get(digit);
                    digit--;
                    switcher = 1;
                } else {
                    switcher = 0;
                }
            }
            baseForTriangle--;
            countOfZero++;
        }
        return pyramid;
    }
}
